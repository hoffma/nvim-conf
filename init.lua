local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = ','
vim.g.localleader = '.'

require("lazy").setup({
    "savq/paq-nvim", -- Let Paq manage itself

    -- for neovim completion
    "neovim/nvim-lspconfig",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/nvim-cmp",
    -- tiny pictograms (for telescope)
    "onsails/lspkind-nvim",
    -- language packs
    "sheerun/vim-polyglot",
    -- "Telescope Requirements
    "nvim-lua/popup.nvim",
    "nvim-lua/plenary.nvim",
    "nvim-telescope/telescope.nvim",
    {"nvim-treesitter/nvim-treesitter", run = function()
	local ts_update = require('nvim-treesitter.install').update({with_sync = true})
	ts_update()
    end},
    {"nvim-telescope/telescope-fzf-native.nvim", run='make' },

    {"kyazdani42/nvim-web-devicons", opt = true },

    "akinsho/bufferline.nvim", -- nice buffer line
    "nvim-lualine/lualine.nvim",

    -- color schemes
    "tjdevries/colorbuddy.vim",
    "Th3Whit3Wolf/onebuddy",
    "NLKNguyen/papercolor-theme",

    -- misc
    "mhinz/vim-startify", -- nice startpage
    "scrooloose/nerdcommenter",
    "raimondi/delimitmate", -- auto completion of quotes, etc.
    "jeffkreeftmeijer/vim-numbertoggle", -- auto toggle relative numbers
    "nvie/vim-flake8", -- python pep8 checker

    "tjdevries/sPoNGe-BoB.NvIm",

    "ellisonleao/gruvbox.nvim",
    {"lervag/vimtex", opt = true }, -- Use braces when passing options
})

require("lazy").setup(plugins, opts)


require('lualine').setup {}
require('bufferline').setup{}
-- setup auto completion plugins and language servers
require('cmp_setup')
-- setup general keymaps
require('keymaps')
-- setup telescope
require('telescope_setup')
-- setup colorscheme
require('colorbuddy').colorscheme('onebuddy')

-- configure treesitter
require('nvim-treesitter.configs').setup {
    ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'tsx', 'typescript', 'vimdoc', 'vim' },
    auto_install = false,
    highlight = { enable = true },
indent = { enable = true },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
      scope_incremental = '<c-s>',
      node_decremental = '<M-space>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
    },
  },
}

-- setup environment
-- vim.cmd('syntax enable')
-- vim.cmd('set background=dark')
-- vim.cmd('colorscheme PaperColor')
vim.o.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme PaperColor]])
-- vim.cmd([[colorscheme gruvbox]])

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.hidden = true
vim.opt.number = true
vim.opt.scrolloff = 10
vim.opt.colorcolumn = "80"
vim.opt.tw = 79
vim.opt.showcmd = true
vim.opt.title = true
vim.opt.history = 10000
vim.opt.laststatus = 2
vim.opt.wrap = true

vim.cmd('highlight ColorColumn ctermbg=6')
vim.cmd('au BufRead,BufNewFile *.vhdl,*.vhd,*.sh,*.py,*.key set wrap linebreak nolist textwidth=0 wrapmargin=0')

vim.cmd('set encoding=UTF-8')

