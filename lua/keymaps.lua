vim.api.nvim_create_autocmd('LspAttach', {
  callback = function(args)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = args.buf })
    vim.keymap.set('n', 'D', vim.diagnostic.open_float, { buffer = args.buf })
    vim.keymap.set('n', 'R', vim.lsp.buf.references, { buffer = args.buf })
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, {buffer = 0})
    vim.keymap.set("n", "df", vim.lsp.buf.format, {buffer = 0})
  end,
})


vim.api.nvim_set_keymap('n', '<leader>h', "<cmd>noh<cr>", {})
-- navigate buffers
vim.api.nvim_set_keymap('n', '<leader>bn', "<cmd>bnext<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>bp', "<cmd>bprevious<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>bf', "<cmd>bfirst<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>bl', "<cmd>blast<cr>", {})
-- close buffer
vim.api.nvim_set_keymap('n', '<leader>bw', ":bw<cr>", {})
-- jump to specific buffer
vim.api.nvim_set_keymap('n', '<leader>1', "<cmd>BufferLineGoToBuffer 1<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>2', "<cmd>BufferLineGoToBuffer 2<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>3', "<cmd>BufferLineGoToBuffer 3<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>4', "<cmd>BufferLineGoToBuffer 4<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>5', "<cmd>BufferLineGoToBuffer 5<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>6', "<cmd>BufferLineGoToBuffer 6<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>7', "<cmd>BufferLineGoToBuffer 7<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>8', "<cmd>BufferLineGoToBuffer 8<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>9', "<cmd>BufferLineGoToBuffer 9<cr>", {})

vim.api.nvim_set_keymap('v', 'J', ":m '>+1<CR>gv=gv", {})
vim.api.nvim_set_keymap('v', 'K', ":m '<-2<CR>gv=gv", {})

-- spongebob case
vim.api.nvim_set_keymap('n', '<leader>st', "<cmd>SpOnGeBoBtOgGlE<cr>", {})

-- vimtex hotkeys
vim.api.nvim_set_keymap('n', '<leader>ll', "<cmd>VimtexCompile<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>lc', "<cmd>VimtexClean<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>lt', "<cmd>VimtexTocToggle<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>lv', "<cmd>VimtexView<cr>", {})
vim.api.nvim_set_keymap('n', '<leader>lo', "<cmd>cclose<cr>", {})

-- vimtex settings
vim.g.vimtex_toc_config = {
  name = 'TOC',
  layers = {'content', 'todo', 'include'},
  resize = 1,
  split_width = 50,
  todo_sorted = 0,
  show_help = 1,
  show_numbers = 1,
  mode = 2,
}

vim.g.tex_flavor = 'latex'

vim.g.vimtex_compiler_latexmk = {
  -- build_dir = 'testbuild',
  -- aux_dir = 'build',
  out_dir = 'build',
  options = {
    '-pdf',
    '-shell-escape',
    '-synctex=1',
  },
}

vim.g.vimtex_view_general_viewer = 'evince'

vim.cmd('set encoding=UTF-8')
