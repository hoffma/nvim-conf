

# Setup

## Requirements

```bash
sudo apt install -y clang clangd clang-format cmake
sudo apt install -y ripgrep python3-pip

sudo python3 -m pip install -U pip
sudo python3 -m pip install cmake-language-server pyright
```
_hint:_ On Ubuntu 22.04 g++-12 has to be installed to so that an issue with
clangd gets fixed ( https://askubuntu.com/questions/1449769/clang-cannot-find-iostream )

GHDL: https://github.com/ghdl/ghdl
GHDL-Language-Server: https://github.com/ghdl/ghdl-language-server

Openscad-lsp: ```cargo install openscad-lsp```

install deno for typescript: https://github.com/denoland/deno

Package Manager used is lazy.nvim https://github.com/folke/lazy.nvim

## Install Nerd Font

- Download font here https://www.nerdfonts.com/
- Unzip and copy to ```~/.local/share/fonts```
- Run the command ```fc-cache -fv ~/.local/share/fonts``` to manually rebuild the font cache
- Set the font in your gnome-terminal

## Old
### Package Manager (Paq-nvim)

Install the package Manager:\
```bash
git clone --depth=1 https://github.com/savq/paq-nvim.git \
    "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/pack/paqs/start/paq-nvim
```
